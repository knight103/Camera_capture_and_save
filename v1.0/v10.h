#pragma once

#include <QtWidgets/QMainWindow>
#include <QtConcurrent/qtconcurrentrun.h>
#include "vidoeplayer.h"
#include <QMainWindow>
#include <QImage>
#include <QPaintEvent>
#include <QWidget>
#include <QtDebug>
#include "V20.h"

namespace Ui {
	class v10;
}
class v10 : public QMainWindow
{
	Q_OBJECT

public:
	explicit v10(QWidget *parent = 0);
	~v10();

private:
	Ui::v10 *ui;

protected:
	void paintEvent(QPaintEvent *event);

private:
	vidoeplayer *mPlayer;                  //播放线程

	QImage mImage;                         //记录当前的图像
	QImage R_mImage;                       //20180614---lizhen

	QString url;

	bool open_red = false;

	V20 *convert_w;

private slots:
	void slotGetOneFrame(QImage img);
	void slotGetRFrame(QImage img);        //20180614---lizhen
	bool slotOpenRed();                    //20180614---lizhen
	bool slotCloseRed();                   //20180614
	void slotOpenConvert();
};
